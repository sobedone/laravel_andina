<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuteticacionRequest;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

class AutenticacionController extends Controller
{
    protected $usuarios;
    public function __construct(User $usuarios)
    {
        $this->usuarios = $usuarios;
        $this->middleware('auth:sanctum')->only('logout');
    }
    public function solicitarToken(Request $request)
    {
        
        $user = User::where('email', $request->get('email'))->first();
        $password = $request->get('password');
        

        if (!$user || !Hash::check($password, $user->password)) {
            throw ValidationException::withMessages([
                'credentials' => ['Las credenciales no coinciden con nuestros registros'],
            ]);
        }

        if ($user->tokens()->get()->count() > 0) {
            $user->tokens()->delete();
        }

        $token = explode('|', $user->createToken('api_test')->plainTextToken)[1];
        return response()->json(['token' => $token], 201);
       
    }

    public function registroUsuario(AuteticacionRequest $authenticationRequest)
    {
        
        $password = $authenticationRequest->get('password');
        $email = $authenticationRequest->get('email');
        $usuarios = new $this->usuarios;
        $usuarios->fill([
            'name' => $authenticationRequest->get('name'),
            'email' => $authenticationRequest->get('email'),
            'password' => $authenticationRequest->get('password'),
        ]);

        $usuarios->save();

        return response()->json(['data' => $usuarios], 201);

    }

    public function logout(Request $request){
       
        $usuario = auth()->user();
        $usuario->tokens()->delete();
        return response()->json(['return' => ''], 200);
    }
}
