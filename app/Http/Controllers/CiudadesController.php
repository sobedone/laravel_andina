<?php

namespace App\Http\Controllers;

use App\Models\Ciudades;
use App\Http\Requests\StoreCiudadesRequest;
use App\Http\Requests\UpdateCiudadesRequest;

class CiudadesController extends Controller
{
    
    protected $ciudades;
    public function __construct(Ciudades $ciudades){
        $this->ciudades = $ciudades;
    }



    public function index (){
        $ciudades = $this->ciudades->all();
        return response()->json(['data' => $ciudades], 200);
        // $this->middleware('auth:sanctum');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCiudadesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCiudadesRequest $request)
    {   
        $ciudades = new  $this->ciudades;
        $ciudades->fill([
            'nombre' => $request->get('nombre'),
            'lat' => $request->get('lat'),
            'long' => $request->get('lng')
        ]);
        $ciudades->save();
        return response()->json(['data' => $ciudades], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ciudades  $ciudades
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ciudades = $this->ciudades->findOrFail($id);
       
        return response()->json(['data' => $ciudades], 202);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCiudadesRequest  $request
     * @param  \App\Models\Ciudades  $ciudades
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCiudadesRequest $request, $id)
    {
        $ciudades = $this->ciudades->findOrFail($id);
        $ciudades->fill([
            'nombre' => $request->get('nombre'),
            'lat' => $request->get('lat'),
            'long' => $request->get('long')
        ]);
        $ciudades->save();

     
        return response()->json(['data' => $ciudades], 202);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ciudades  $ciudades
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ciudades = $this->ciudades->findOrFail($id);
      
        $ciudades->delete();
        return response()->json(['data' => $ciudades], 202);
    }
}
