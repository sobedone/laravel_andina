<?php

namespace App\Http\Controllers;

use App\Models\Productos;
use App\Http\Requests\StoreProductosRequest;
use App\Http\Requests\UpdateProductosRequest;
use Illuminate\Http\Request;
class ProductosController extends Controller
{




    protected $productos;
    public function __construct(Productos $productos){
        $this->productos = $productos;
        $this->middleware('auth:sanctum');
    }
   

    public function index(){
        // $fileName =  \Storage::url('archivo.pdf');
        // dd($fileName);
        $producto =  $this->productos->with('ciudades')->get();
        return response()->json(['data' => $producto], 200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProductosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductosRequest $request)
    {   
        
        // header("Access-Control-Allow-Origin: *");
        // header("Access-Control-Allow-Headers: ACCEPT, CONTENT-TYPE, X-CSRF-TOKEN");
        // header("Access-Control-Allow-Methods: GET, POST, OPTIONS, DELETE");
            
        $producto = new $this->productos;

        $imagen = $request->file('imagen');
        $fileName   = time() . $imagen->getClientOriginalName();
        
        \Storage::disk('productos')->put($fileName, \File::get($imagen));
        $filePath =  asset('storage/productos/'.$fileName);
       
        $ciudadId = json_decode( $request->get('ciudad_id'));
        


        $producto->fill([
            'nombre' => $request->get('nombre'),
            'precio' => $request->get('precio'),
            'cantidad' => $request->get('cantidad'),
            'imagen' => $filePath,
            'observaciones' => $request->get('observaciones')
        ]);

        $producto->save();
        $producto->ciudades()->sync($ciudadId);
        return response()->json(['data' => $producto], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto = $this->productos->getProducto($id);
        return response()->json(['data' => $producto], 200);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProductosRequest  $request
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // header("Access-Control-Allow-Origin: *");
        $producto = $this->productos->findOrFail($id);
        
        $imagen = $request->file('imagen');
        if(is_null($imagen)){
            $filePath = $producto->imagen;
        }else{
            $fileName   = time() . $imagen->getClientOriginalName();
            \Storage::disk('productos')->put($fileName, \File::get($imagen));
            $filePath =  asset('storage/productos/'.$fileName);
            $ciudadId = json_decode( $request->get('ciudad_id'));
        }

        $ciudadId = json_decode( $request->get('ciudad_id'));
        $producto->fill([
            'nombre' => $request->get('nombre'),
            'precio' => $request->get('precio'),
            'cantidad' => $request->get('cantidad'),
            'imagen' => $filePath,
            'observaciones' => $request->get('observaciones')
        ]);
        $producto->save();
        $producto->ciudades()->sync($ciudadId);
        return response()->json(['data' => $producto], 202);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $producto = $this->productos->findOrFail($id);
        
        $producto->delete();
        return response()->json(['data' => $producto], 202);
    }
}
