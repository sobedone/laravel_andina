<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       

        return [
            'nombre' => 'required',
            'precio' => 'required',
            'cantidad' => 'required',
            'imagen' => 'required'.$this->id,
            'observaciones' => 'required',
            
            
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El :attributes es obligatorio.',
            'precio.required' => 'La :attributes es obligatorio',
            'cantidad.required' => 'El :attributes es obligatorio.',
            'imagen.required' => 'La :attributes es obligatorio',
            'observaciones.required' => 'La :attributes es obligatorio',
            
            
        ];
    }
    public function attributes()
    {
        return [
            'nombre' => 'Nombre del producto',
            'precio' => 'Precio del producto',
            'cantidad' => 'Cantidad del producto',
            'imagen' => 'Imagen del producto',
            'observaciones' => 'Observaciones del producto',
        ];
    }
}
