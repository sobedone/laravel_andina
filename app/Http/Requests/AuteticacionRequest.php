<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuteticacionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'email' => 'required|unique:users,email',
            'password' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'El :attributes es obligatorio.',
            'email.unique' => 'El :attributes ya se encuentra en uso',
            'password.required' => 'El :attributes es obligatorio.'
        ];
    }

    public function attributes()
    {
        return [
            'email' => 'Correo',
            'password' => 'Contraseña',
           
        ];
    }
}
