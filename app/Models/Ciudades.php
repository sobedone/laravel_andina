<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ciudades extends Model
{
    use HasFactory;

    protected $table = 'ciudades';
    protected $fillable = [
        'nombre',
        'lat',
        'long'
      
    ];

    public function productos()
    {
        return $this->belongsToMany('App\Models\Productos', 'ciudades_productos', 'ciudad_id', 'producto_id');
    }
}
