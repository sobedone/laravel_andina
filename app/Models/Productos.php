<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    use HasFactory;

    protected $table = 'productos';

    protected $fillable = [
        'nombre',
        'precio',
        'cantidad',
        'imagen',
        'observaciones',
    ];

    public function ciudades()
    {
        return $this->belongsToMany('App\Models\Ciudades', 'ciudades_productos', 'producto_id', 'ciudad_id');
    }

    public function getProducto($id)
    {
        $producto = $this->leftJoin('ciudades_productos', 'productos.id', '=', 'ciudades_productos.producto_id')
            ->leftJoin('ciudades', 'ciudades.id', '=', 'ciudades_productos.ciudad_id')
            ->where('productos.id', '=', $id)
            ->select([
                'ciudades.id AS ciudad_id',
                'ciudades.nombre AS nombre_ciudad',
                'ciudades.lat',
                'ciudades.long',
                'productos.nombre AS producto_nombre',
                'productos.precio',
                'productos.cantidad',
                'productos.imagen',
                'productos.observaciones',
                'productos.id as producto_id',
            ])
            ->get();
         
            $data =[];
            foreach ($producto as $key => $value) {
                $data['nombre']= $value->producto_nombre;
                $data['precio']= $value->precio;
                $data['cantidad']= $value->cantidad;
                $data['imagen']= $value->imagen;
                $data['observaciones']= $value->observaciones;
                if (!is_null($value->nombre_ciudad)) {
                    $data['ciudades'][]= $value->nombre_ciudad;
                    $data['ciudades_id'][]= $value->ciudad_id;
                    $data['latitudes'][] =[
                        'lat' => $value->lat,
                        'long' => $value->long,
                    ];
                    # code...
                }else{
                    $data['ciudades'] =[];
                    $data['ciudades_id']=[];
                    $data['latitudes']=[];
                }
            }
        return $data;
    }
}
