<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;

trait HandlerResponse {

    /**
     * General success response
     * @param string $data
     * @param int $code
     * @return JsonResponse
     */
    protected function success (string $data, int $code)
    {
        return response()->json($data, $code);
    }

    /**
     * General error response
     * @param string $message
     * @param int $code
     * @return JsonResponse
     */
    protected function error ( string $message, int $code )
    {
        return response()->json(['error' => $message, 'code'=> $code], $code);
    }

}

