<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        
        return [
            'nombre'=>$this->faker->name(),
            'precio'=>$this->faker->randomDigit,
            'cantidad'=>$this->faker->numberBetween($min = 10, $max = 100),
            'imagen' =>$this->faker->imageUrl($width = 200, $height = 200),
            'observaciones'=>$this->faker->paragraph,
         
        ];
    }
}
