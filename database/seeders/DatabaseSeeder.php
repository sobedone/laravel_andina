<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Ciudades;
use App\Models\Productos;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        Productos::factory(10)->create();
        Ciudades::factory(6)->create();
      

        Productos::factory(10)->create()->each(
            function ($producto) {
         
                $ciudadesRandom = Ciudades::inRandomOrder()->first();
                $producto->ciudades()->attach($ciudadesRandom->id);
            }
        );
        // \App\Models\User::factory(10)->create();
    }
}
