<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Ciudades;
use App\Models\Productos;
class CiudadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        
        factory(Ciudades::class, 6)->create();

        factory(Productos::class, 6)->create()->each(
            function ($producto) {
                $producto->ciudades()->attach($producto->id);
            }
        );
    }
}
