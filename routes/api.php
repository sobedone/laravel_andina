<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductosController;
use App\Http\Controllers\CiudadesController;
use App\Http\Controllers\AutenticacionController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('/productos', ProductosController::class);
Route::apiResource('/ciudades', CiudadesController::class);

Route::Post('/usuarios', [AutenticacionController::class,'registroUsuario']);
Route::Post('/usuarios/token', [AutenticacionController::class,'solicitarToken']);
Route::get('/logout', [AutenticacionController::class,'logout']);